package com.msoftware.autocompramobile.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompramobile.entity.ProdutoEntity;
import com.msoftware.autocompramobile.repository.ProdutoRepository;
import com.msoftware.autocompramobile.service.interfaces.ProdutoService;
import com.msoftware.autocompramobile.vo.ProdutoVO;

@Repository
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repo;

		
	@Override
	public List<ProdutoVO> findAllProdutos() {
		List<ProdutoEntity> list = (List<ProdutoEntity>) this.repo.findAllProdutos();
		List<ProdutoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}
	
	private ProdutoVO fromVO(ProdutoEntity entity) {
		ProdutoVO objVO = new ProdutoVO(entity.getId(), entity.getCodBarrasProd(), entity.getDescProd(), entity.getPrecoUnit(), entity.getPesoProd());
		return objVO;

	}

	@Override
	public List<ProdutoVO> findProdByCodBarra(Long codBarra) {
		List<ProdutoEntity> list = (List<ProdutoEntity>) this.repo.findProdByCodBarra(codBarra);
		List<ProdutoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

}

package com.msoftware.autocompramobile.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompramobile.entity.VendasEntity;
import com.msoftware.autocompramobile.repository.VendasRepository;
import com.msoftware.autocompramobile.service.interfaces.VendasService;
import com.msoftware.autocompramobile.vo.VendasVO;

@Repository
public class VendasServiceImpl implements VendasService {

	@Autowired
	private VendasRepository repo;

	public List<VendasVO> findAllVendas() {
		List<VendasEntity> list = (List<VendasEntity>) this.repo.findAllVendas();
		List<VendasVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	private VendasVO fromVO(VendasEntity entity) {
		VendasVO objVO = new VendasVO(entity.getId(), entity.getIdCliente(), entity.getCodBarrasProd(),
				entity.getIdItemProd(), entity.getPrecoUnit(), entity.getValorTotal(), entity.getDataVenda());
		return objVO;

	}

	@Override
	public void insert(VendasVO objVO) {
		VendasEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private VendasEntity fromEntity(VendasVO objVO) {
		return new VendasEntity(objVO.getId(), objVO.getIdCliente(), objVO.getCodBarrasProd(),
				objVO.getIdItemProd(), objVO.getPrecoUnit(), objVO.getValorTotal(), objVO.getDataVenda() );
	}


	
}

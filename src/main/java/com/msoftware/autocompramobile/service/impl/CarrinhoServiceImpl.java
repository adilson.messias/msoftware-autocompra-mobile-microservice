package com.msoftware.autocompramobile.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompramobile.entity.CarrinhoEntity;
import com.msoftware.autocompramobile.repository.CarrinhoRepository;
import com.msoftware.autocompramobile.service.interfaces.CarrinhoService;
import com.msoftware.autocompramobile.vo.CarrinhoVO;

@Repository
public class CarrinhoServiceImpl implements CarrinhoService {

	@Autowired
	private CarrinhoRepository repo;

	@Override
	public void insert(CarrinhoVO objVO) {
		CarrinhoEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private CarrinhoEntity fromEntity(CarrinhoVO objVO) {
		return new CarrinhoEntity(objVO.getId(), objVO.getCodBarrasCarrinho(), objVO.getIdItemProd(),
				                  objVO.getPrecoUnit(), objVO.getQtdAdicionada(), objVO.getValorTotal(),  
		                          objVO.getPesoCarrinho(), objVO.getData() );
	}

	public void deleteById(Integer id) {
		this.repo.delete(id);
		
	}

	@Override
	public List<CarrinhoVO> findAllCarrinho() {
		List<CarrinhoEntity> list = (List<CarrinhoEntity>) this.repo.findAllCarrinho();
		List<CarrinhoVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}
	
	private CarrinhoVO fromVO(CarrinhoEntity entity) {
		CarrinhoVO objVO = new CarrinhoVO(entity.getId(), entity.getCodBarrasCarrinho(), entity.getIdItemProd(),
				entity.getPrecoUnit(), entity.getQtdAdicionada(), entity.getValorTotal(),  
				entity.getPesoCarrinho(), entity.getData());
	return objVO;

	}
	
}

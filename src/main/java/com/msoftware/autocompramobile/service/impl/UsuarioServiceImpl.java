package com.msoftware.autocompramobile.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.msoftware.autocompramobile.entity.UsuarioEntity;
import com.msoftware.autocompramobile.repository.UsuarioRepository;
import com.msoftware.autocompramobile.service.interfaces.UsuarioService;
import com.msoftware.autocompramobile.vo.UsuarioVO;

@Repository
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository repo;

	@Override
	public void insert(UsuarioVO objVO) {
		UsuarioEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	private UsuarioEntity fromEntity(UsuarioVO objVO) {
		return new UsuarioEntity(objVO.getEmail(), objVO.getSenha(), objVO.getId());

	}

	private UsuarioVO fromVO(UsuarioEntity entity) {
		UsuarioVO objVO = new UsuarioVO(entity.getEmail(), entity.getSenha(), entity.getId());
		return objVO;

	}

	@Override
	public List<UsuarioVO> findUserByEmail(String email) {
		List<UsuarioEntity> list = (List<UsuarioEntity>) this.repo.findUserByEmail(email);
		List<UsuarioVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

	@Override
	public void update(UsuarioVO objVO) {
		UsuarioEntity entity = fromEntity(objVO);
		this.repo.save(entity);

	}

	
	public List<UsuarioVO> findAllUsers() {
		List<UsuarioEntity> list = (List<UsuarioEntity>) this.repo.findAllUsers();
		List<UsuarioVO> listObjVO = list.stream().map(entity -> fromVO(entity)).collect(Collectors.toList());
		return listObjVO;
	}

}

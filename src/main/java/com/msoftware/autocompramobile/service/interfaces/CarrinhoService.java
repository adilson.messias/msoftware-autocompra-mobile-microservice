package com.msoftware.autocompramobile.service.interfaces;

import java.util.List;

import com.msoftware.autocompramobile.vo.CarrinhoVO;

public interface CarrinhoService {

	public void insert(CarrinhoVO objVO);

	public List<CarrinhoVO> findAllCarrinho();
}

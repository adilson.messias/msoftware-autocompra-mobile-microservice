package com.msoftware.autocompramobile.service.interfaces;

import java.util.List;

import com.msoftware.autocompramobile.vo.VendasVO;

public interface VendasService {

	public List<VendasVO> findAllVendas();

	public void insert(VendasVO objVO);
}

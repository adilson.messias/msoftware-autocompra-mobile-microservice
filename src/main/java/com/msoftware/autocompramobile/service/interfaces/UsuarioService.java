package com.msoftware.autocompramobile.service.interfaces;

import java.util.List;

import com.msoftware.autocompramobile.vo.UsuarioVO;

public interface UsuarioService {
	
	public void insert(UsuarioVO objVO);
	
	public List<UsuarioVO> findUserByEmail(String email);
	
	public void update(UsuarioVO objVO);
	
	
}

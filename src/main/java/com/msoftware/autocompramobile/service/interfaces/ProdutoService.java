package com.msoftware.autocompramobile.service.interfaces;

import java.util.List;

import com.msoftware.autocompramobile.vo.ProdutoVO;

public interface ProdutoService {

	
	public List<ProdutoVO> findAllProdutos();

	public List<ProdutoVO> findProdByCodBarra(Long codBarra);

	
}

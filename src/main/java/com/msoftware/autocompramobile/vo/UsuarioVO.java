package com.msoftware.autocompramobile.vo;

public class UsuarioVO {
	private Integer id;
	private String email;
	private String senha;
	
	
	public UsuarioVO() {
		
	}
	
	
	public UsuarioVO(String email, String senha, Integer id) {
		super();
		this.email = email;
		this.senha = senha;
		this.id = id;

	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	

}

package com.msoftware.autocompramobile.vo;

import java.util.Date;

public class VendasVO {

	private Integer id;
	private Integer idCliente;
	private Long codBarrasProd;
	private Integer idItemProd;
	private Double precoUnit;
	private Double valorTotal;
	private Date dataVenda;

	VendasVO() {
	}

	public VendasVO(Integer id, Integer idCliente, Long codBarrasProd, Integer idItemProd, Double precoUnit,
			Double valorTotal, Date dataVenda) {
		super();
		this.id = id;
		this.codBarrasProd = codBarrasProd;
		this.idItemProd = idItemProd;
		this.precoUnit = precoUnit;
		this.valorTotal = valorTotal;
		this.dataVenda = dataVenda;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public Integer getIdItemProd() {
		return idItemProd;
	}

	public void setIdItemProd(Integer idItemProd) {
		this.idItemProd = idItemProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	

}

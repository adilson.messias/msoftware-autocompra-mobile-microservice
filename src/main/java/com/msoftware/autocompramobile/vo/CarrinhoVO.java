package com.msoftware.autocompramobile.vo;

import java.util.Date;

public class CarrinhoVO {

	private Integer id;
	private Long codBarrasCarrinho;
	private Integer idItemProd;
	private Double precoUnit;
	private Integer qtdAdicionada;
	private Double valorTotal;
	private Integer pesoCarrinho;
	private Date data;

	CarrinhoVO() {
	}

	public CarrinhoVO(Integer id, Long codBarrasCarrinho, Integer idItemProd, Double precoUnit,
			Integer qtdAdicionada, Double valorTotal, Integer pesoCarrinho, Date data) {
		super();
		this.id = id;
		this.codBarrasCarrinho = codBarrasCarrinho;
		this.idItemProd = idItemProd;
		this.precoUnit = precoUnit;
		this.qtdAdicionada = qtdAdicionada;
		this.valorTotal = valorTotal;
		this.pesoCarrinho = pesoCarrinho;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasCarrinho() {
		return codBarrasCarrinho;
	}

	public void setCodBarrasCarrinho(Long codBarrasCarrinho) {
		this.codBarrasCarrinho = codBarrasCarrinho;
	}

	public Integer getIdItemProd() {
		return idItemProd;
	}

	public void setIdItemProd(Integer idItemProd) {
		this.idItemProd = idItemProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Integer getQtdAdicionada() {
		return qtdAdicionada;
	}

	public void setQtdAdicionada(Integer qtdAdicionada) {
		this.qtdAdicionada = qtdAdicionada;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getPesoCarrinho() {
		return pesoCarrinho;
	}

	public void setPesoCarrinho(Integer pesoCarrinho) {
		this.pesoCarrinho = pesoCarrinho;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}

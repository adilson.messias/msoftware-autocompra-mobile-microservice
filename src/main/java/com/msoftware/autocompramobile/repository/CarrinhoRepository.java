package com.msoftware.autocompramobile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msoftware.autocompramobile.entity.CarrinhoEntity;


public interface CarrinhoRepository extends CrudRepository<CarrinhoEntity, Integer> {

	@Query(nativeQuery = true, value = "select * from tb_carrinho_fisico")
	public List<CarrinhoEntity> findAllCarrinho();

	
	
}

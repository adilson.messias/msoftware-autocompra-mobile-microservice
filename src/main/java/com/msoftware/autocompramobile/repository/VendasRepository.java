package com.msoftware.autocompramobile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.msoftware.autocompramobile.entity.VendasEntity;


public interface VendasRepository extends CrudRepository<VendasEntity, Integer> {

	
	@Query(nativeQuery = true, value = "select * from tb_vendas")
	public List<VendasEntity> findAllVendas();

	
	
}

package com.msoftware.autocompramobile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.msoftware.autocompramobile.entity.ProdutoEntity;


public interface ProdutoRepository extends CrudRepository<ProdutoEntity, Integer> {

	@Query(nativeQuery = true, value = "select * from tb_produto")
	public List<ProdutoEntity> findAllProdutos();

	@Query(nativeQuery = true, value = "select * from tb_produto where cod_barra_prod in(:codBarra)")
	public List<ProdutoEntity> findProdByCodBarra(@Param("codBarra") Long codBarra);

}

package com.msoftware.autocompramobile.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompramobile.service.impl.UsuarioServiceImpl;
import com.msoftware.autocompramobile.vo.UsuarioVO;

@RestController
@RequestMapping(value = "/autocompra/usuario")
public class Usuario {

	@Autowired
	private UsuarioServiceImpl usuarioService;

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestHeader("usuario") String usuario, @RequestBody UsuarioVO objVO) {
		this.usuarioService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<UsuarioVO>> findUserByEmail(UsuarioVO objVO) {
		String email = objVO.getEmail();
		List<UsuarioVO> listObjVO = this.usuarioService.findUserByEmail(email);
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<List<UsuarioVO>> update(@RequestBody UsuarioVO objVO, @PathVariable Integer id) {
		objVO.setId(id);
		this.usuarioService.update(objVO);
		return ResponseEntity.noContent().build();

	}

	
}

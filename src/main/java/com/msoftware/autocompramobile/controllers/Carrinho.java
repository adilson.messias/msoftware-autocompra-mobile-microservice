package com.msoftware.autocompramobile.controllers;

import java.net.URI;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompramobile.service.impl.CarrinhoServiceImpl;
import com.msoftware.autocompramobile.vo.CarrinhoVO;
import com.msoftware.autocompramobile.vo.ProdutoVO;


@RestController
@RequestMapping(value = "/autocompra/carrinho")
public class Carrinho {
	
	@Autowired
	private CarrinhoServiceImpl carrinhoService;
	
	
	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestBody CarrinhoVO objVO) {
		this.carrinhoService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<ProdutoVO>> deleteById(@PathVariable Integer id) {
		this.carrinhoService.deleteById(id);
		return ResponseEntity.noContent().build();

	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<CarrinhoVO>> findAllCarrinho() {
		List<CarrinhoVO> listObjVO = this.carrinhoService.findAllCarrinho();
		return ResponseEntity.ok().body(listObjVO);

	}
}

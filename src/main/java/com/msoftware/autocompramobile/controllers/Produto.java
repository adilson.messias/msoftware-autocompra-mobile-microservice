package com.msoftware.autocompramobile.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.msoftware.autocompramobile.service.impl.ProdutoServiceImpl;
import com.msoftware.autocompramobile.vo.ProdutoVO;

@RestController
@RequestMapping(value = "/autocompra/produto")
public class Produto {
	
	@Autowired
	private ProdutoServiceImpl produtoService;

	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoVO>> findAllProdutos() {
		List<ProdutoVO> listObjVO = this.produtoService.findAllProdutos();
		return ResponseEntity.ok().body(listObjVO);

	}

	@RequestMapping(method = RequestMethod.OPTIONS)
	public ResponseEntity<List<ProdutoVO>> findProdutoByCodBarra(ProdutoVO objVO) {
		Long codBarra = objVO.getCodBarrasProd();
		List<ProdutoVO> listObjVO = this.produtoService.findProdByCodBarra(codBarra);
		return ResponseEntity.ok().body(listObjVO);

	}

	
}

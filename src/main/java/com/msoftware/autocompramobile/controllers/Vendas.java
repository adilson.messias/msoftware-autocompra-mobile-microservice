package com.msoftware.autocompramobile.controllers;

import java.net.URI;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.msoftware.autocompramobile.service.impl.VendasServiceImpl;
import com.msoftware.autocompramobile.vo.VendasVO;

@RestController
@RequestMapping(value = "/autocompra/vendas")
public class Vendas {
	
	@Autowired
	private VendasServiceImpl vendasService;

	@RequestMapping(method = RequestMethod.POST)
	@Transactional
	public ResponseEntity<Void> insert(@RequestBody VendasVO objVO) {
		this.vendasService.insert(objVO);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(uri).build();

	}

	

}

package com.msoftware.autocompramobile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_carrinho_fisico")
@SequenceGenerator(name = "car", sequenceName = "seq_car", initialValue = 1)
public class CarrinhoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car")
	@Column(name = "id")
	private Integer id;

	@Column(name = "cod_barra_car")
	private Long codBarrasCarrinho;

	@Column(name = "id_item_prod")
	private Integer idItemProd;

	@Column(name = "preco_unit")
	private Double precoUnit;

	@Column(name = "qtd_add")
	private Integer qtdAdicionada;

	@Column(name = "valor_total")
	private Double valorTotal;

	@Column(name = "peso_car")
	private Integer pesoCarrinho;

	@Column(name = "data")
	private Date data;

	public CarrinhoEntity() {

	}

	public CarrinhoEntity(Integer id, Long codBarrasCarrinho, Integer idItemProd, Double precoUnit,
			Integer qtdAdicionada, Double valorTotal, Integer pesoCarrinho, Date data) {
		super();
		this.id = id;
		this.codBarrasCarrinho = codBarrasCarrinho;
		this.idItemProd = idItemProd;
		this.precoUnit = precoUnit;
		this.qtdAdicionada = qtdAdicionada;
		this.valorTotal = valorTotal;
		this.pesoCarrinho = pesoCarrinho;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasCarrinho() {
		return codBarrasCarrinho;
	}

	public void setCodBarrasCarrinho(Long codBarrasCarrinho) {
		this.codBarrasCarrinho = codBarrasCarrinho;
	}

	public Integer getIdItemProd() {
		return idItemProd;
	}

	public void setIdItemProd(Integer idItemProd) {
		this.idItemProd = idItemProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Integer getQtdAdicionada() {
		return qtdAdicionada;
	}

	public void setQtdAdicionada(Integer qtdAdicionada) {
		this.qtdAdicionada = qtdAdicionada;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getPesoCarrinho() {
		return pesoCarrinho;
	}

	public void setPesoCarrinho(Integer pesoCarrinho) {
		this.pesoCarrinho = pesoCarrinho;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}

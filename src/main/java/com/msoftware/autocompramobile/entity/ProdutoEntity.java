package com.msoftware.autocompramobile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_produto")
@SequenceGenerator(name = "prod", sequenceName = "seq_prod", initialValue = 1)
public class ProdutoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prod")
	@Column(name = "id")
	private Integer id;

	@Column(name = "cod_barra_prod")
	private Long codBarrasProd;

	@Column(name = "desc_prod")
	private String descProd;

	@Column(name = "preco_unit")
	private Double precoUnit;

	@Column(name = "peso_prod")
	private Double pesoProd;

	public ProdutoEntity() {

	}

	public ProdutoEntity(Integer id, Long codBarrasProd, String descProd, Double precoUnit, Double pesoProd) {
		super();
		this.id = id;
		this.codBarrasProd = codBarrasProd;
		this.descProd = descProd;
		this.precoUnit = precoUnit;
		this.pesoProd = pesoProd;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getCodBarrasProd() {
		return codBarrasProd;
	}

	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}

	public String getDescProd() {
		return descProd;
	}

	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}

	public Double getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}

	public Double getPesoProd() {
		return pesoProd;
	}

	public void setPesoProd(Double pesoProd) {
		this.pesoProd = pesoProd;
	}

}

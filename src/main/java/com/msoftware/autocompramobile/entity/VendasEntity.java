package com.msoftware.autocompramobile.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tb_vendas")
@SequenceGenerator(name = "vendas", sequenceName = "seq_vendas", initialValue = 1)
public class VendasEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendas")
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "id_cliente")
	private Integer idCliente;
	
	@Column(name = "cod_barra_prod")
	private Long codBarrasProd;

	@Column(name = "id_item_prod")
	private Integer idItemProd;

	@Column(name = "preco_unit")
	private Double precoUnit;

	@Column(name = "valor_total")
	private Double valorTotal;

	@Column(name = "data_venda")
	private Date dataVenda;
	
	
	public VendasEntity() {

	}


	public VendasEntity(Integer id, Integer idCliente, Long codBarrasProd, Integer idItemProd, Double precoUnit,
			Double valorTotal, Date dataVenda) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.codBarrasProd = codBarrasProd;
		this.idItemProd = idItemProd;
		this.precoUnit = precoUnit;
		this.valorTotal = valorTotal;
		this.dataVenda = dataVenda;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}


	public Long getCodBarrasProd() {
		return codBarrasProd;
	}


	public void setCodBarrasProd(Long codBarrasProd) {
		this.codBarrasProd = codBarrasProd;
	}


	public Integer getIdItemProd() {
		return idItemProd;
	}


	public void setIdItemProd(Integer idItemProd) {
		this.idItemProd = idItemProd;
	}


	public Double getPrecoUnit() {
		return precoUnit;
	}


	public void setPrecoUnit(Double precoUnit) {
		this.precoUnit = precoUnit;
	}


	public Double getValorTotal() {
		return valorTotal;
	}


	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}


	public Date getDataVenda() {
		return dataVenda;
	}


	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	

}

package com.msoftware.autocompramobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoCompraMobileApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoCompraMobileApplication.class, args);
	}

}
